import tkinter as Tk
 
 
personality_index = {"lion": 0, "otter" : 0, "retriever" : 0, "beaver" : 0}
 
 
class Question:
    """Create a class called Question that will be used to implement a personality
    quiz """
 
    def __init__(self, question, answers):
       """Create a __init__method that allows user to do inital set up of object.
 
        Args:
           self: refers to current object, allows us to modify/access properties of an instance
           question: refers to the question being called from text file
           answers: refers to the answers that go with the question being called
 
        """
 
       self.question = question
       self.answers = answers
 
    def check(self, letter, view):
        """Checks the user input and adds +1 to designated personality type in dictionary index.
 
        Args:
          letter(str): Letter user chooses in personality quiz
          view(event): unpacks view of dictionary index in .050s  
        Side Effects:
          Modifies dictionary    
        """
        global = right 
        if(letter == "A"):
            personality_index["lion"] += 1
        if(letter == "B"):
            personality_index["otter"] +=1
        if(letter == "C"):
            personality_index["retriever"] += 1
        else:
            personality_index["beaver"] += 1
        view.after(50, lambda *args: self.unpackView(view))
 
  
    def getView(self, window):
        """Creates the widgets for the users to click on and returns view.
        args:
            window(event): creates geometry and customizes layout
        Return:
            view: returns view to user of letter chosen
        """
 
        view = Frame(window)
    
        Label(view, text=self.question, font = "Verdana 15 bold").pack()
        Button(view, text=self.answers[0], font = "Verdana 13", bg = "#8FD3DE", command=lambda *args: self.check("A", view)).pack()
        Button(view, text=self.answers[1], font = "Verdana 13", bg = "#8FD3DE", command=lambda *args: self.check("B", view)).pack()
        Button(view, text=self.answers[2], font = "Verdana 13", bg = "#8FD3DE", command=lambda *args: self.check("C", view)).pack()
        Button(view, text=self.answers[3], font = "Verdana 13", bg = "#8FD3DE", command=lambda *args: self.check("D", view)).pack()
        return view
 
    def unpackView(self, view):
        """Hides view from user and asks user question.
        args:
            view(event): view of window frame
        """
 
        view.pack_forget()
        askQuestion()
      
    def askQuestion(self):
        """ define a function that prints the users calculated personality type
        Args: no arguments taken
        global questions, window, index, button, right, number of questions
        """
 
        global questions, window, index, button, right, number_of_questions
        if(len(questions) == index + 1):
            keymax = max(personality_index, key = personality_index.get)
      
        if keymax == "lion":
            Label(window, font = "Verdana 13", text="Thank you for answering the questions. \n You got " + max(personality_index, key=personality_index.get) + "! Lions are leaders, they are decisive. \n They love to solve problems. They seek new adventures and opportunities.").pack()
        if keymax == "otter":
            Label(window, font = "Verdana 13", text="Thank you for answering the questions. \n You got " + max(personality_index, key=personality_index.get) +"! Otters are excitable, fun-seeking and love to talk. \n They are great at motivating others and are extremely outgoing. They are very loving, and encouraging.").pack()
        if keymax == "retriever":
            Label(window, font = "Verdana 13", text="Thank you for answering the questions. \n You got " + max(personality_index, key=personality_index.get) +"! Retrievers are loyal. \n They can absorb the most emotional pain and punishment in a relationship and still stay commited. \n They are great listeners.").pack()
        if keymax == "beaver":
            Label(window, font = "Verdana 13", text="Thank you for answering the questions. \n You got " + max(personality_index, key=personality_index.get) +"! Beavers have a strong need to do things correctly. \n They are the kind of people who actually read instruction manuals.").pack()
 
    button.pack_forget()
    index += 1
    questions[index].getView(window).pack()
  
    def first(self, question, answers):
        """ A function that will iterate through each group of answers in the file
    
            Args:
                self: refers to current object, allows us to modify/access
                properties of an instance
                question: refers to the question being called from text file
                answers: refers to the answers that go with the question being called
        """
 
        questions = []
        with open('questions.txt', 'r', encoding = "utf-8") as file:
            line = file.readline()
        while(line != ""):
            questionString = line
        answers = []
        for i in range (4):
            answers.append(file.readline())
        space = file.readline()
        space = space[:-1]
        questions.append(Question(questionString, answers))
        line = file.readline()
 
    def close(self):
        """ Closes the question file and creates a variable to hold the length
        of questions """
        file.close()
        index = -1
        number_of_questions = len(questions)
    
    def interface(self, button):
        """ uses tkinter to make interface larger and have color. Initilizes button
        that is going to start the quiz 
        """
        window = Tk()
        window.title("Personality Quiz")
        window.configure(background = "gray")
        window.geometry("1000x600")
        button = Button(window, text="Start Personality Quiz", font = "Verdana 16 bold", command=askQuestion)
        button.pack()
        window.mainloop()

def main(self):
    q = Question(question, answers)
    

if __name__ == '__main__':
    finalcode.main()