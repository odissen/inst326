import tkinter as tk
from tkinter import *

class Question():
    def __init__(self,window):
        """Set attributes.
        Args:
            window(event): creates geometry and customizes layout
        Attributes:
            personality_index(dict): holds value of user input answers
            questionnumber(int): value of 1
            frame(event): organzie and group widgets
            answers(dict): dictionary for answers to quiz
            question(str): question for user to answer
        """ 


        self.personality_index = {"lion": 0, "otter" : 0, "retriever" : 0, "beaver" : 0}
        self.questionnumber=1 
        self.window = window
        self.frame = tk.Frame(self.window)
        self.window.configure(background = "gray")
        self.window.geometry("1000x600")
        
        self.answers = []
        self.question = "Which Describes You ?"
        self.first(self.questionnumber)
        
        self.a1 = Button(self.window,text=self.answers[0], font = "Verdana 13",
                         command= lambda:self.check('A'))
        self.a2 = Button(self.window,text=self.answers[1], font = "Verdana 13",
                         command= lambda:self.check('B'))
        self.a3 = Button(self.window,text=self.answers[2], font = "Verdana 13",
                         command= lambda:self.check('C'))
        self.a4 = Button(self.window,text=self.answers[3], font = "Verdana 13",
                         command= lambda:self.check('D'))
        
        self.back = Button(self.window, text="BACK BUTTON",
                           command= lambda:self.back_question())
        self.startButton = Button(self.window,text="Start Personality Quiz", 
                             font = "Verdana 16 bold",command=self.start_quiz)
        self.startButton.pack()
        self.first(self.questionnumber)
    
    def start_quiz(self):
        """A function that starts the personality quiz, generates a view of the quiz,
    packs the widgets.  
    Args:
        self
        """    
        self.getView()

        self.a1.pack()
        self.a2.pack()
        self.a3.pack()
        self.a4.pack()
        self.startButton.pack_forget()
        
        
    def back_question(self):
        """A function that allows the user to go back to questions; check the number 
        of question the user is at by seeing if it is equal to one or equal to or 
        less than 2. 
        Args:
        self
        Attribute:
        questionnumber(int): value of 1
        """
        if (self.questionnumber == 1):
            None
        else:
            self.questionnumber-=2
            self.check("back")


    def next_question(self):
        """A function that allows the user to go onto the next question; adds 
        one to the number of questions if the number of questions is less than
        11 continues to ask questions; else gives user results.
        Args:
        self
        keymax(str): types of animals
        """
        self.questionnumber+=1 
        self.first(self.questionnumber)
        
        self.a1 = Button(self.window,text=self.answers[0], font = "Verdana 13",
                         command= lambda:self.check('A'))
        self.a2 = Button(self.window,text=self.answers[1], font = "Verdana 13",
                         command= lambda:self.check('B'))
        self.a3 = Button(self.window,text=self.answers[2], font = "Verdana 13",
                         command= lambda:self.check('C'))
        self.a4 = Button(self.window,text=self.answers[3], font = "Verdana 13",
                         command= lambda:self.check('D'))
        if self.questionnumber < 11:
            self.a1.pack()
            self.a2.pack()
            self.a3.pack()
            self.a4.pack()
        else:
            self.askQuestion()
            
    def askQuestion(self):
        """A function that asks user question and gives result 
        Arg:
        keymax(str): type of animal
        """
        keymax = max(self.personality_index, key = self.personality_index.get)
        print(keymax)
        if keymax == "lion":
            Label(self.window, font = "Verdana 13", text="Thank you for answering the questions. \n You got " 
                  + max(self.personality_index, key=self.personality_index.get) + 
                  "! Lions are leaders, they are decisive. \n They love to solve problems. They seek new adventures and opportunities.").pack()
        if keymax == "otter":
            Label(self.window, font = "Verdana 13", text="Thank you for answering the questions. \n You got " 
                  + max(self.personality_index, key=self.personality_index.get) +
                  "! Otters are excitable, fun-seeking and love to talk. \n They are great at motivating others and are extremely outgoing. They are very loving, and encouraging.").pack()
        if keymax == "retriever":
            Label(self.window, font = "Verdana 13", text="Thank you for answering the questions. \n You got "
                   + max(self.personality_index, key=self.personality_index.get) +
                   "! Retrievers are loyal. \n They can absorb the most emotional pain and punishment in a relationship and still stay commited. \n They are great listeners.").pack()
        if keymax == "beaver":
            Label(self.window, font = "Verdana 13", text="Thank you for answering the questions. \n You got " 
                  + max(self.personality_index, key=self.personality_index.get) +
                  "! Beavers have a strong need to do things correctly. \n They are the kind of people who actually read instruction manuals.").pack()
 

        
        
        
    def check(self, letter):
        """Checks the user input and adds +1 to designated personality type in
        dictionary index.
 
        Args:
            self
            letter(str): Letter user chooses in personality quiz 
        Side Effects:
          Modifies dictionary    
        """
        self.a1.pack_forget()
        self.a2.pack_forget()
        self.a3.pack_forget()
        self.a4.pack_forget()


        if(letter == "A"):
            self.personality_index["lion"] += 1
        if(letter == "B"):
            self.personality_index["otter"] +=1
        if(letter == "C"):
            self.personality_index["retriever"] += 1
        if(letter =="back"):
            pass
        else:
            self.personality_index["beaver"] += 1

        self.next_question()
        

    def getView(self):
        """Creates the widgets for the users to click on and returns view.
        Args:
            self """
        self.back.place(x=130,y=550,anchor=S,height=100, width = 300)


        Questionz = Label(self.window, text=self.question, font = "Verdana 26 bold")
        Questionz.pack()
 

    
    def first(self,questionnumber):
        """ Creates a dictionary that contains the answers and reads in the question
        text file. Assigns question number to question line in text file
            Args:
            self
            questionnumber: a int that contains the question number """
            
        self.answers = []
        with open('questions.txt', 'r', encoding = "utf-8") as file:
            if questionnumber == 1:
                file.readline()
            if questionnumber == 2:
                for i in range(7):
                    file.readline()
            if questionnumber == 3:
                for i in range(13):
                    file.readline()
            if questionnumber == 4:
                for i in range(19):
                    file.readline()
            if questionnumber == 5:
                for i in range(25):
                    file.readline()
            if questionnumber == 6:
                for i in range(31):
                    file.readline()
            if questionnumber == 7:
                for i in range(37):
                    file.readline()
            if questionnumber == 8:
                for i in range(43):
                    file.readline()
            if questionnumber == 9:
                for i in range(49):
                    file.readline()
            if questionnumber == 10:
                for i in range(55):
                    file.readline()
            for i in range(4):
                self.answers.append(file.readline())

    
        
def main():
    """Instantiates an object of Question class and runs through a GUI
        Returns:
        Runs quiz"""
    root = tk.Tk()
    app = Question(root)
    root.mainloop()
                             
if __name__ == '__main__':
    main()