 
class Question:
   """Create a class called Question that will be used to implement a personality
    quiz """"
  
   def __init__(self, question, answers):
   """Create a __init__method that allows user to do inital set up of object
       Args:
       self: refers to current object, allows us to modify/access properties of an instance
       question: refers to the question being called from text file
       answers: refers to the answers that go with the question being called
   """
 
def check (self, letter, view):
   """Checks the user input and adds +1 to designated personality type in dictionary index.
       Args:
           letter(str): Letter user chooses in personality quiz
           view(event): unpacks view of dictionary index in .050s   
 
       Side Effects:
           Modifies dictionary     
   """
 
def getView(self, window):
   """Creates the widgets for the users to click on and returns view.
       args:
           window(event): creates geometry and customizes layout
 
       Return:
           view: returns view to user of letter chosen
   """
def unpackView(self,view):
   """Hides view from user and asks user question.
       args:
           view(event): view of window frame
   """
def askQuestion():
     """ define a function that prints the users calculated personality type 
        Args: no arguments taken 
        global questions, window, index, button, right, number of questions
    """
"""Create an empty dictionary to hold the questions"""
"""Open and read the .txt file with questions"""
 
def first(self, question, answers):
       """ A function that will iterate through each group of answers in the file
      
           Args:
               self: refers to current object, allows us to modify/access
               properties of an instance
               question: refers to the question being called from text file
               answers: refers to the answers that go with the question being called
       """
 def close(self):
       """ Closes the question file and creates a variable to hold the length
        of questions """"
  
def interface(self, button):
  
   """ uses tkinter to make interface larger and have color. Initilizes button
   that is going to start the quiz """
